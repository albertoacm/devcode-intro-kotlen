import java.util.*

/**
 * Created by administrador on 30/05/17.
 */
enum class Pokemones(val id: Int){ BALBASAUR(1), CHARMANDER(2), SQUIRTLE(3), PIKACHU(4) }

fun main(args: Array<String>){
    println("Elige un pokemon:")
    for ((index,p) in Pokemones.values().withIndex()){
        println("${index + 1}) $p")
    }

    val scanner = Scanner(System.`in`)
    val opcion = scanner.nextInt()

    val pokemon : Pokemon = generaPokemon(opcion)

    //println(pokemon.nombre)
    mostrarPokemon(pokemon)

    val random = Random()
    val numeroAlAzar = 1 + random.nextInt(4)
    val pokemonSalvaje: Pokemon = generaPokemon(numeroAlAzar)

    println("Un ${pokemonSalvaje.nombre} salvaje \n")

    do{
        println("${pokemon.nombre} HP:${pokemon.hp} | ${pokemonSalvaje.nombre} HP:${pokemonSalvaje.hp}")
        println("Elige un ataque:")

        for((index,a) in pokemon.listaAtaques.withIndex()){
            println("${index}: ${a.nombre}")

            val ataqueSeleccionado = scanner.nextInt()

            if(procesarAtaque(pokemon, pokemonSalvaje, ataqueSeleccionado)){
                break
            }

            val ataqueAleatorio: Int = 1 + random.nextInt(pokemonSalvaje.listaAtaques.size)

            if(procesarAtaque(pokemonSalvaje, pokemon, ataqueAleatorio)){
                break
            }else{
                println("Los dos pokemones siguen en pie")
                println("...continuamos")
            }

        }
    }while (pokemon.hp > 0 && pokemonSalvaje.hp > 0)

}

fun procesarAtaque(pokemonAtacante: Pokemon, pokemonDefensor: Pokemon, ataqueSeleccionado: Int): Boolean{
    val ataque = pokemonAtacante.obtenerAtaque(ataqueSeleccionado)
    println("${pokemonAtacante.nombre} ha usado ${ataque.nombre}")

    val valorDanio = calcularDanio(pokemonAtacante.ataque, pokemonAtacante.defensa, ataque)
    println("${pokemonDefensor.nombre} ha recibido $valorDanio de daño!")
    pokemonDefensor.hp -= valorDanio

    if(pokemonDefensor.hp <= 0){
        println("${pokemonDefensor.nombre} se agotó!")
        println("${pokemonAtacante.nombre} ganó la batalla!")

        return true
    }

    return false
}

fun calcularDanio(valorAtaque: Int, valorDefensa: Int, ataque: Ataque): Double = ((((2 * 1 + 10) / 250) * (valorAtaque / valorDefensa) * ataque.poder + 2) * 1.5 )

fun mostrarPokemon(pokemon: Pokemon){
    println("Haz elegido a: ${pokemon.nombre} \nHP:${pokemon.hp} \nATAQUE:${pokemon.ataque} \nDefensa:${pokemon.defensa} ")
}

fun generaPokemon(opcion: Int): Pokemon = when(opcion){
    Pokemones.BALBASAUR.id -> Pokemon("Balbasaur",45.0,49,49, arrayOf(Ataque("vine wip",45), Ataque("tackle",40)))
    Pokemones.CHARMANDER.id -> Pokemon("Charmander",39.0,52,43, arrayOf(Ataque("scratch",40), Ataque("ember",40)))
    Pokemones.SQUIRTLE.id -> Pokemon("Squirtle",44.0,48,65, arrayOf(Ataque("tackle",40), Ataque("watergun",40)))
    Pokemones.PIKACHU.id -> Pokemon("Pikachu",35.0,55,40, arrayOf(Ataque("thunder shock",40), Ataque("quick attack",45)))
    else -> Pokemon("Missingno",33.0,136,0, arrayOf(Ataque("pay day",20), Ataque("blind",15)))
}